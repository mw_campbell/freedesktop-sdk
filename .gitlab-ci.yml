variables:
  # Store everything under the /builds directory. This is a separate Docker
  # volume. Note that GitLab CI will only cache stuff inside the build
  # directory.
  XDG_CACHE_HOME: "${CI_PROJECT_DIR}/cache"
  GET_SOURCES_ATTEMPTS: 3
  RUNTIME_VERSION: '24.08'
  STABLE_ABI: 'false'
  EXPIRED: '0'
  MAX_MERGE_REQUESTS: '4'
  CACHE_PUSH: 'true'

  # Docker Images
  DOCKER_REGISTRY: "registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images"
  DOCKER_IMAGE_ID: "d4e5a7b4138f2ebfbee7f412e325f3cfbda0da1c"

  # Generic variable for invoking buildstream
  BST: bst --colors

  # Make sure python bytecode doesn't mix up with sources
  PYTHONPYCACHEPREFIX: "${XDG_CACHE_HOME}/python"

  GIT_SUBMODULE_STRATEGY: "recursive"
  BUILD_TASKS: 4
  BUILD_CPU_MULTIPLIER: 1

default:
  image: "${DOCKER_REGISTRY}/bst2:${DOCKER_IMAGE_ID}"
  interruptible: true
  after_script:
  - make clean
  - rm -rf "${XDG_CACHE_HOME}/buildstream/artifacts"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/build"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/cas"
  - rm -rf "${XDG_CACHE_HOME}/buildstream/sources"
  - rm -rf "${CI_PROJECT_DIR}/.bst"
  - rm -rf "${CI_PROJECT_DIR}/.flatpak_builder"
  retry:
    max: 2
    when: runner_system_failure

workflow:
  rules:
  - if: '$CI_MERGE_REQUEST_IID'
  - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == "true"'
  - if: '$CI_COMMIT_TAG'
  - if: '$CI_PIPELINE_SOURCE == "schedule"'
  - if: '$CI_PIPELINE_SOURCE == "web"'

stages:
  - bootstrap
  - update
  - flatpak
  - snap
  - vm
  - prepare-publish
  - publish
  - finish-publish
  - reproducible

before_script:
  - ulimit -n 1048576
  - export PATH=~/.local/bin:${PATH}

  - |
    # Force higher parallelity
    export MAX_JOBS=$(python -c "print(round(($(nproc) * ${BUILD_CPU_MULTIPLIER}) / ${BUILD_TASKS}))")
    mkdir -p ~/.config
    [ -f /cache-certificate/server.crt ] && export LOCAL_CAS=true
    python3 -mmako.cmd templates/buildstream.conf --output-file ~/.config/buildstream.conf
    cat ~/.config/buildstream.conf

  # flat-manager tokens to upload the releases
  - |
    if [ -z "$(grep BRANCH= Makefile | grep beta)" ]
    then
      export RELEASE_CHANNEL=stable
    else
      export RELEASE_CHANNEL=beta
    fi

    if [ -n "$CI_COMMIT_TAG" ] && [ -n "$FLATHUB_REPO_TOKEN" ]; then
        export RELEASES_SERVER_ADDRESS=https://hub.flathub.org/
        export REPO_TOKEN="${FLATHUB_REPO_TOKEN}"
        ./utils/generate-version flatpak-version.yml
        case "${CI_COMMIT_TAG}" in
          *rc*) ;&
          *beta*)
            test "$RELEASE_CHANNEL" = "beta"
            ;;
          *)
            test "$RELEASE_CHANNEL" = "stable"
            # Check we have enabled stable ABI before we do any stable release
            [ "${STABLE_ABI}" = "true" ]
            ;;
        esac
    elif [ -n "$RELEASES_REPO_TOKEN" ]; then
        export REPO_TOKEN=$RELEASES_REPO_TOKEN
        export RELEASES_SERVER_ADDRESS=https://releases.freedesktop-sdk.io/
        # We always use "stable" here. This is all beta on this server.
        export RELEASE_CHANNEL=stable
    fi

  - |
    if [ -n "$CI_COMMIT_TAG" ] && [ -n "$SNAPCRAFT_LOGIN_FILE" ]; then
        case "${CI_COMMIT_TAG}" in
          *beta*)
            export SNAP_RELEASE=beta
            export SNAP_GRADE=devel
            ;;
          *rc*)
            export SNAP_RELEASE=candidate
            export SNAP_GRADE=stable
            ;;
          *)
            export SNAP_RELEASE=stable
            export SNAP_GRADE=stable
            ;;
        esac
        ./utils/generate-version snap-version.yml
    elif [ -n "$SNAPCRAFT_LOGIN_FILE" ]; then
        export SNAP_RELEASE=edge
        export SNAP_GRADE=devel
        ./utils/generate-version snap-version.yml
    else
        export SNAP_GRADE=devel
    fi

  - |
    if [ -n "$CI_COMMIT_TAG" ]; then
        case "${CI_COMMIT_TAG}" in
            *rc*) ;&
            *beta*)
                export DOCKER_VERSION="${RUNTIME_VERSION}-beta"
                ;;
            *)
                export DOCKER_VERSION="${RUNTIME_VERSION}"
                ;;
        esac
    else
        export DOCKER_VERSION="${RUNTIME_VERSION}-devel"
    fi

pylint:
  stage: flatpak
  script:
    - find . -path 'files/oci' -prune -o -iname "*.py" -exec pylint {} +
    - pylint files/oci/oci_builder
  except:
    - master
    - '/^release\/.*/'
    - tags
    - schedules
  needs: []

.check_update_elements:
  stage: update
  variables:
    GIT_SUBMODULE_STRATEGY: none
  script:
    - git remote set-url origin "https://gitlab-ci-token:${FREEDESKTOP_API_KEY}@gitlab.com/freedesktop-sdk/freedesktop-sdk.git"
    - git checkout -- snap-version.yml
    - git config user.name "freedesktop_sdk_updater"
    - git config user.email "freedesktop_sdk_updater@libreml.com"
    - git branch -f "${CI_COMMIT_REF_NAME}" "origin/${CI_COMMIT_REF_NAME}"
    - |
      case "${CI_COMMIT_REF_NAME}" in
        master)
        ;;
        release/*)
        ;;
        *)
          false
        ;;
      esac
    - |
      if [ "$SCHEDULE_TASK" = "daily" ] && [ "$STABLE_ABI" = "true" ]; then
          TRACK_ELEMENTS="$TRACK_ELEMENTS abi/reference-abi.bst"
      fi
      GITLAB_TOKEN=$FREEDESKTOP_API_KEY auto_updater --verbose \
      --base_branch "${CI_COMMIT_REF_NAME}" \
      --nobuild \
      --overwrite \
      --push \
      --create_mr \
      --shuffle-branches \
      --gitlab_project="freedesktop-sdk/freedesktop-sdk" \
      --max_merge_requests=${MAX_MERGE_REQUESTS} \
      $EXTRA_TRACK_ARGS \
      $TRACK_ELEMENTS
  interruptible: false

daily_update_elements:
  extends:
    - .check_update_elements
  variables:
    TRACK_ELEMENTS: track-elements.bst
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK == "daily"'

weekly_update_elements:
  extends:
    - .check_update_elements
  variables:
    TRACK_ELEMENTS: track-elements-weekly.bst
    EXTRA_TRACK_ARGS: >-
      --bstignore=/dev/null
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK == "weekly"'

check_mirrors:
  stage: update
  before_script:
  # Configure Buildstream
  - mkdir -p ~/.config
  - |
    cat > ~/.config/buildstream.conf << EOF
    # Get a lot of output in case of errors
    logging:
      message-format: '[%{wallclock}][%{elapsed}][%{key}][%{element}] %{action} %{message}'
      error-lines: 80
    scheduler:
      network-retries: 4
      on-error: continue
    # We want to test that everything is mirrored
    fetch:
      source: mirrors
    source-caches:
      override-project-caches: true
    EOF

  script:
    - |
      ${BST} source fetch --deps all flatpak-release-repo.bst components.bst \
             components/rust-stage1-{x86_64,i686,aarch64,powerpc64le}.bst \
             oci/layers/{flatpak,debug,platform,sdk}.bst
  allow_failure: true
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK == "daily"'

license_checker:
  extends:
    - .builder-x86_64
  stage: update
  needs: []
  variables:
    DEPS: all
    TARGETS: manifests/release-url-manifest.bst
    OUTPUT_DIR: ${CI_PROJECT_DIR}/buildstream_license_checker_output
    WORK_DIR: ${CI_PROJECT_DIR}/buildstream_license_checker_working_directory
    IGNORE_LIST: files/bst_license_checker_ignorelist
  artifacts:
    when: always
    paths:
      - ${XDG_CACHE_HOME}/buildstream/logs
      - ${OUTPUT_DIR}
  cache:
    key: license_checker
    paths:
      - ${WORK_DIR}
  script:
    - |
      ${BST} build ${TARGETS}
      bst_license_checker --deps "${DEPS}" -w ${WORK_DIR} -o ${OUTPUT_DIR} --ignorelist ${IGNORE_LIST} ${TARGETS}
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK == "weekly"'

.flatpak_template:
  stage: flatpak
  script:
    - test -x arch-test && arch-test
    - ./utils/validate_aliases.py include/_private/aliases.yml include/_private/mirrors.yml
    - test "${ARCH}" != "i686" && make build

    - make check-debuginfo

    - export FLATPAK_USER_DIR="${PWD}/tmp-flatpak"
    - make test-apps

    - git checkout -- snap-version.yml
    - |
      ABI_SKIP=$(git log -n 1 | grep -E "\[abi skip\]|\[skip abi\]" || true)
      if [ "${STABLE_ABI}" != "false" ]; then
        if [ -z "${ABI_SKIP}" ]; then
          make check-abi
        else
          echo ABI check skipped
        fi
      fi
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
      - ${CI_PROJECT_DIR}/libabigail-tars
      - ${CI_PROJECT_DIR}/mirror_problem_list.json
  except:
    - master
    - '/^release\/.*/'
    - tags
    - schedules

.builder-x86_64:
  tags:
    - x86_64
  variables:
    ARCH: x86_64
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2

.builder-i686:
  tags:
    - x86_64
  variables:
    ARCH: i686
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2

.builder-aarch64:
  tags:
    - aarch64
  variables:
    ARCH: aarch64
    BUILD_TASKS: 4
    BUILD_CPU_MULTIPLIER: 1

.builder-riscv64:
  tags:
    - riscv
  variables:
    ARCH: riscv64
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2

.builder-ppc64le:
  tags:
  - ppc64le
  variables:
    ARCH: ppc64le
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 1

.builder-ppc64:
  tags:
  - x86_64
  variables:
    ARCH: ppc64
    BUILD_TASKS: 2
    BUILD_CPU_MULTIPLIER: 2

app_x86_64:
  extends:
    - .flatpak_template
    - .builder-x86_64
  needs: []

app_i686:
  extends:
    - .flatpak_template
    - .builder-i686
  needs: []

app_aarch64:
  extends:
    - .flatpak_template
    - .builder-aarch64
  needs: []

app_riscv64:
  extends:
    - .flatpak_template
    - .builder-riscv64
  allow_failure: true
  when: manual
  needs: []

app_ppc64le:
  extends:
    - .flatpak_template
    - .builder-ppc64le
  needs: []

app_ppc64:
  extends:
    - .flatpak_template
    - .builder-ppc64
  allow_failure: true
  when: manual
  needs:
  - bootstrap_ppc64

.check-snap:
  stage: snap
  script:
    - make export-snap
    - python3 utils/parse_review.py snap/sdk.snap
    - python3 utils/parse_review.py snap/platform.snap
    - python3 utils/parse_review.py snap/glxinfo.snap
    - python3 utils/parse_review.py snap/vulkaninfo.snap
    - python3 utils/parse_review.py snap/clinfo.snap
    - python3 utils/parse_review.py snap/vainfo.snap
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  when: manual
  allow_failure: true
  except:
    - master
    - '/^release\/.*/'
    - tags
    - schedules

check_snap_x86_64:
  extends:
    - .check-snap
    - .builder-x86_64

check_snap_i686:
  extends:
    - .check-snap
    - .builder-i686

.bootstrap_template:
  stage: bootstrap
  script:
    - make bootstrap
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - '/^release\/.*/'
    - tags
    - schedules

bootstrap_loongarch64:
  extends:
    - .bootstrap_template
    # We're short on x86_64 runners.
    - .builder-aarch64
  allow_failure: true
  when: manual
  variables:
    ARCH: loongarch64

bootstrap_ppc64:
  extends:
    - .bootstrap_template
    - .builder-x86_64
  allow_failure: true
  when: manual
  variables:
    ARCH: ppc64

.vm_imageless_template:
  stage: vm
  script:
    - make build-vm
    - |
      if [ "${DIALOG+set}" = set ]; then
        utils/test_minimal_system.py --dialog "${DIALOG}" command 'make run-vm'
      fi
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '/^release\/.*/'
    - tags
    - schedules

minimal_systemd_vm_x86_64:
  extends:
    - .vm_imageless_template
    - .builder-x86_64
  variables:
    VM_ARTIFACT_BOOT: vm/boot/virt.bst
    VM_ARTIFACT_FILESYSTEM: vm/minimal/virt.bst
    DIALOG: root-login
  needs:
    - job: app_x86_64
      artifacts: false
  when: manual # skip x86_64 VM tests by default; the runner is too slow

minimal_systemd_vm_aarch64:
  extends:
    - .vm_imageless_template
    - .builder-aarch64
  variables:
    VM_ARTIFACT_BOOT: vm/boot/virt.bst
    VM_ARTIFACT_FILESYSTEM: vm/minimal/virt.bst
    DIALOG: root-login
  needs:
    - job: app_aarch64
      artifacts: false

minimal_systemd_vm_loongarch64:
  extends:
    - .vm_imageless_template
    - .builder-x86_64
  variables:
    VM_ARTIFACT_BOOT: vm/boot/virt.bst
    VM_ARTIFACT_FILESYSTEM: vm/minimal/virt.bst
    ARCH: loongarch64
  allow_failure: true
  when: manual
  needs:
    - job: bootstrap_loongarch64
      artifacts: false

desktop_systemd_vm_x86_64:
  extends:
    - .vm_imageless_template
    - .builder-x86_64
  variables:
    VM_ARTIFACT_BOOT: vm/boot/virt.bst
    VM_ARTIFACT_FILESYSTEM: vm/desktop/virt.bst
  needs:
    - job: app_x86_64
      artifacts: false
  when: manual # skip x86_64 VM tests by default; the runner is too slow

desktop_systemd_vm_aarch64:
  extends:
    - .vm_imageless_template
    - .builder-aarch64
  variables:
    VM_ARTIFACT_BOOT: vm/boot/virt.bst
    VM_ARTIFACT_FILESYSTEM: vm/desktop/virt.bst
  needs:
    - job: app_aarch64
      artifacts: false

.vm_efi_image_template:
  stage: vm
  script:
    - make build-efi-vm
    - |
      if [ "${DIALOG+set}" = set ]; then
        utils/test_minimal_system.py --dialog "${DIALOG}" command 'make run-efi-vm'
      fi
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  except:
    - master
    - '/^release\/.*/'
    - tags
    - schedules

minimal_efi_vm_image_x86_64:
  extends:
    - .vm_efi_image_template
    - .builder-x86_64
  needs:
  - app_x86_64
  variables:
    VM_ARTIFACT_IMAGE: vm/minimal/efi.bst
    DIALOG: root-login
  when: manual # skip x86_64 VM tests by default; the runner is too slow

minimal_efi_vm_image_aarch64:
  extends:
    - .vm_efi_image_template
    - .builder-aarch64
  needs:
  - app_aarch64
  variables:
    VM_ARTIFACT_IMAGE: vm/minimal/efi.bst
    DIALOG: root-login

minimal_efi_vm_image_loongarch64:
  extends:
    - .vm_efi_image_template
    - .builder-x86_64
  needs:
  - minimal_systemd_vm_loongarch64
  variables:
    VM_ARTIFACT_IMAGE: vm/minimal/efi.bst
    ARCH: loongarch64
  allow_failure: true
  when: manual

desktop_efi_vm_image_x86_64:
  extends:
    - .vm_efi_image_template
    - .builder-x86_64
  needs:
  - app_x86_64
  variables:
    VM_ARTIFACT_IMAGE: vm/desktop/efi.bst
  when: manual # skip desktop image VM tests by default to avoid significant disk I/O

desktop_efi_vm_image_aarch64:
  extends:
    - .vm_efi_image_template
    - .builder-aarch64
  needs:
  - app_aarch64
  variables:
    VM_ARTIFACT_IMAGE: vm/minimal/efi.bst
  when: manual # skip desktop image VM tests by default to avoid significant disk I/O

prepare_publish:
  stage: prepare-publish
  script:
    - flat-manager-client -v create "$RELEASES_SERVER_ADDRESS" "${RELEASE_CHANNEL}" --build-log-url ${CI_PIPELINE_URL} > publish_build.txt
  artifacts:
    paths:
      - publish_build.txt
  except:
    - schedules
  when: delayed
  start_in: 1 minute
  interruptible: false
  only:
    - master
    - '/^release\/.*/'
    - tags

finish_publish:
  stage: finish-publish
  script:
    - |
      if [ "${EXPIRED}" = "1" ]; then
        EOL_MESSAGE=$(utils/eol-message.py ${RUNTIME_VERSION})
        flat-manager-client -v commit --wait "$(cat publish_build.txt)" \
          --end-of-life "${EOL_MESSAGE}"
      else
        flat-manager-client -v commit --wait "$(cat publish_build.txt)"
      fi
    - flat-manager-client -v publish --wait-update "$(cat publish_build.txt)"
    - flat-manager-client -v purge "$(cat publish_build.txt)"
    - make manifest
  artifacts:
     paths:
      - "${CI_PROJECT_DIR}/platform-manifest/usr/"
      - "${CI_PROJECT_DIR}/sdk-manifest/usr/"
  only:
    - tags
    - master
    - '/^release\/.*/'
  except:
    - schedules
  needs:
    - prepare_publish
    - job: publish_x86_64
      artifacts: false
    - job: publish_i686
      artifacts: false
    - job: publish_aarch64
      artifacts: false

finish_publish_failed:
  stage: finish-publish
  script:
    - flat-manager-client purge "$(cat publish_build.txt)"
  only:
    - tags
    - master
    - '/^release\/.*/'
  except:
    - schedules
  when: on_failure
  needs:
    - prepare_publish
    - job: publish_x86_64
      artifacts: false
    - job: publish_i686
      artifacts: false
    - job: publish_aarch64
      artifacts: false

.flatpak_publish_template:
  stage: publish
  retry: 2
  script:
    - test -f publish_build.txt
    - make export

    - |
      case "$RELEASES_SERVER_ADDRESS" in
        https://hub.flathub.org/)
          for ref in $(ostree --repo=repo refs --list); do
            case "${ref}" in
              */*/ppc64le/*) ;&
              */*/i386/*)
                echo "Deleting ${ref}"
                ostree --repo=repo refs --delete "${ref}"
                ;;
              */org.freedesktop.Sdk*/*/*) ;&
              */org.freedesktop.Platform*/*/*)
                echo "Keeping ${ref}"
                ;;
              */*/*/*)
                echo "Deleting ${ref}"
                ostree --repo=repo refs --delete "${ref}"
                ;;
            esac
          done
          ;;
        https://releases.freedesktop-sdk.io/)
          ;;
        *)
          false
          ;;
      esac

    - flatpak build-update-repo --generate-static-deltas repo
    - flat-manager-client push "$(cat publish_build.txt)" repo --build-log-url ${CI_PIPELINE_URL}

  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule"'
    when: never
  - if: $CI_COMMIT_TAG
    when: on_success
  - if: '$CI_COMMIT_BRANCH =~ /^release\/.*/'
    when: on_success
  - if: '$CI_COMMIT_BRANCH == "master"'
    when: on_success
  needs:
    - prepare_publish

.publish_tar_template:
  stage: publish
  script:
    - make export-tar
    - |
      aws --endpoint-url "${AWS_ENDPOINT}" \
          s3 cp --recursive --acl public-read \
                "${CI_PROJECT_DIR}/tarballs" \
                "s3://freedesktop-sdk-tarballs/$(git describe)"
  only:
    - tags
  except:
    - schedules

publish_x86_64:
  extends:
    - .flatpak_publish_template
    - .builder-x86_64

publish_i686:
  extends:
    - .flatpak_publish_template
    - .builder-i686

publish_aarch64:
  extends:
    - .flatpak_publish_template
    - .builder-aarch64

publish_x86_64_tar:
  extends:
    - .publish_tar_template
    - .builder-x86_64
  needs: []

publish_i686_tar:
  extends:
    - .publish_tar_template
    - .builder-i686
  needs: []

publish_aarch64_tar:
  extends:
    - .publish_tar_template
    - .builder-aarch64
  needs: []
  when: manual

cve_report:
  stage: finish-publish
  cache:
    key: cve
    paths:
      - "${XDG_CACHE_HOME}/cve"
  script:
    - make generate-cve-report

  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/cve-reports"
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK != "daily"'
    when: never
  - if: '$CI_COMMIT_BRANCH == "master"'
  - if: '$CI_COMMIT_BRANCH =~ /^release\/.*/'
  needs: []

markdown_manifest:
  stage: finish-publish
  script:
    - make markdown-manifest
  artifacts:
    paths:
      - "${CI_PROJECT_DIR}/platform-manifest/usr/"
      - "${CI_PROJECT_DIR}/sdk-manifest/usr/"
  only:
    - master
    - '/^release\/.*/'
  except:
    - schedules
  needs: []

.reproducible_template:
  stage: reproducible
  script:
    - ./utils/test_reproducibility.py tests/reproducible-test.bst result_folder
  needs: []
  variables:
    DISABLE_REMOTE_CACHE: "1"
  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
      - ${CI_PROJECT_DIR}/result_folder               # All results of diffoscope for non-reproducible builds.
      - ${CI_PROJECT_DIR}/reproducibility_results.html # All elements that are not reproducible
  interruptible: false
  rules:
  - if: '$CI_PIPELINE_SOURCE == "schedule" && $SCHEDULE_TASK == "weekly"'

reproducible_aarch64:
  extends:
    - .reproducible_template
    - .builder-aarch64

reproducible_x86_64:
  extends:
    - .reproducible_template
    - .builder-x86_64
  when: manual
  allow_failure: true

.snap_publish_template:
  stage: publish
  script:
    - |
      [ -n "${SNAPCRAFT_LOGIN_FILE}" ]
    - |
      [ -n "${SNAP_RELEASE}" ]

    - make export-snap

    - mkdir -p ".snapcraft/"
    - echo "${SNAPCRAFT_LOGIN_FILE}" | base64 --decode --ignore-garbage > ".snapcraft/snapcraft.cfg"

    - |
      push() {
        if ! snapcraft push "${1}" --release "${SNAP_RELEASE}" 2>&1 | tee output.log; then
          cmp -s <(tail -n2 output.log) - <<EOF
      Please check the errors and some hints below:
        - (NEEDS REVIEW) type 'base' not allowed
      EOF
        fi
      }
      failed=false
      for snap in platform sdk glxinfo vulkaninfo clinfo vainfo; do
        if ! push "snap/${snap}.snap"; then
          failed=true
        fi
      done
      [ "${failed}" == false ]

  artifacts:
    when: always
    paths:
      - ${CI_PROJECT_DIR}/cache/buildstream/logs
  only:
    - tags
    - master
  except:
    - schedules

publish_snap_x86_64:
  extends:
    - .snap_publish_template
    - .builder-x86_64
  when: manual

publish_snap_i686:
  extends:
    - .snap_publish_template
    - .builder-i686
  when: manual

publish_snap_aarch64:
  extends:
    - .snap_publish_template
    - .builder-aarch64
  when: manual

.docker_publish_template:
  stage: publish
  retry: 2
  script:
  - |
    [ -n "${DOCKER_HUB_USER}" ]
  - |
    [ -n "${DOCKER_HUB_PASSWORD}" ]
  - |
    [ -n "${DOCKER_HUB_ADDRESS}" ]

  - |
    DOCKER_IMAGES=(platform sdk debug flatpak toolbox)
    BST_ELEMENTS=$(printf 'oci/%q-docker.bst ' ${DOCKER_IMAGES[@]})
    
    ${BST} -o target_arch ${ARCH} build ${BST_ELEMENTS[@]}
    for element in ${BST_ELEMENTS[@]}; do
       ${BST} -o target_arch ${ARCH} artifact checkout ${element} --tar - | podman load
    done

  - podman login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}" "${DOCKER_HUB_ADDRESS}"

  - |
    set -eu
    for name in ${DOCKER_IMAGES[@]}; do
      full_name=freedesktopsdk/${name}
      podman push ${full_name}:latest docker://docker.io/${full_name}:"${DOCKER_VERSION}"-"${ARCH}" \
      --digestfile digest.txt
      mkdir -p "docker-pushed-digests/${DOCKER_VERSION}"
      digest=$(<digest.txt)
      echo "${full_name}@${digest}" \
          >"docker-pushed-digests/${DOCKER_VERSION}/${name}-${ARCH}.txt"
    done

  artifacts:
    when: always
    paths:
    - ${CI_PROJECT_DIR}/cache/buildstream/logs
    - docker-pushed-digests
  only:
  - tags
  except:
  - schedules

publish_docker_x86_64:
  extends:
    - .builder-x86_64
    - .docker_publish_template
  variables:
    CACHE_PUSH: 'false'
  needs: []

publish_docker_i686:
  extends:
    - .builder-i686
    - .docker_publish_template
  variables:
    CACHE_PUSH: 'false'
  needs: []

publish_docker_aarch64:
  extends:
    - .builder-aarch64
    - .docker_publish_template
  variables:
    CACHE_PUSH: 'false'
  needs: []


publish_docker_ppc64le:
  extends:
    - .builder-ppc64le
    - .docker_publish_template
  variables:
    CACHE_PUSH: 'false'
  needs: []

publish_docker_finish:
  stage: finish-publish
  allow_failure: true
  before_script: []
  script:
  - podman login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}" "${DOCKER_HUB_ADDRESS}"

  - |
    set -eu
    
    LATEST_RELEASE=$(./utils/detect_latest.py)
    
    for name in platform sdk debug flatpak; do
      for dir in docker-pushed-digests/*; do
        DOCKER_VERSION="$(basename "${dir}")"
        podman manifest create "freedesktopsdk/${name}:${DOCKER_VERSION}" \
                      $(cat "${dir}"/${name}-*.txt)
        podman manifest push --format v2s2 \
                      "freedesktopsdk/${name}:${DOCKER_VERSION}" \
                      docker://docker.io/freedesktopsdk/"${name}:${DOCKER_VERSION}"
        if [ "${RUNTIME_VERSION}" = "${LATEST_RELEASE}" ]
        then
          podman manifest push --format v2s2 \
                        "freedesktopsdk/${name}:${DOCKER_VERSION}" \
                        docker://docker.io/freedesktopsdk/"${name}:latest"
        fi
      done
    done

  only:
  - tags
  except:
  - schedules
  needs:
  - publish_docker_aarch64
  - publish_docker_x86_64
  - publish_docker_i686
  - publish_docker_ppc64le
